<?php
session_start();
// include 'conexion.php';
include 'conexionDebo.php';

if ($_SERVER["REQUEST_METHOD"] == "POST") {
    if (isset($_POST['numero_temporada'])) {
        $numero_temporada = $_POST['numero_temporada'];
    // Obtener el ID de la serie y el número de temporada enviado desde el formulario
    $id_serie = $_POST['id_serie'];
    $numero_temporada = $_POST['numero_temporada'];

    // Verificar si el campo numero_temporada está vacío
    if (empty($numero_temporada)) {
        echo "<p>Por favor, ingrese el número de temporada.</p>";
    } else {
        // Si el campo numero_temporada no está vacío, proceder con la inserción en la base de datos
        $sql = "INSERT INTO temporadas (id_serie, numero_temporada) VALUES ('$id_serie', '$numero_temporada')";

        // Ejecutar la consulta
        if (mysqli_query($conn, $sql)) {
            header("Location: series_opciones.php");
            exit();
        } else {
            echo "<p>Error al añadir Temporada: " . mysqli_error($conn) . "</p>";
        }
    }} else {
        echo "<p>Por favor, ingrese el número de temporada.</p>";
    }

    
    // Cerrar la conexión a la base de datos
    mysqli_close($conn);
}
?>

<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <title>Añadir Temporada</title>
    <link rel="stylesheet" href="css/style.css">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0/css/all.min.css" rel="stylesheet"/>
    <link rel="stylesheet" href="css/añadir.css">
    <link rel="stylesheet" href="css/boton.css">
</head>
<body>
    <h1>Añadir Temporada</h1>
    <form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method="post" enctype="multipart/form-data">
        <label for="numero_temporada">Numero de Temporada:</label>
        <input type="text" id="numero_temporada" name="numero_temporada" required><br>
        
        <!-- Campo oculto para enviar el ID de la serie -->
        <input type="hidden" name="id_serie" value="<?php echo isset($_POST['id_serie']) ? $_POST['id_serie'] : ''; ?>">
        
        <input type="submit" value="Añadir">
        
    </form>
    <button><a href="series_opciones.php">Volver</a></button>
</body>
</html>
