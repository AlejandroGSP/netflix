<?php
session_start();
// include 'conexion.php';
include 'conexionDebo.php';

// Mostrar mensaje de error si está presente en la sesión
if (isset($_SESSION['error_borrar'])) {
    echo "<p>{$_SESSION['error_borrar']}</p>";
    unset($_SESSION['error_borrar']); // Limpiar el mensaje para evitar que se muestre de nuevo en futuras recargas de la página
}

if(isset($_POST['submit_borrar']) && $_POST['submit_borrar'] == "borrar") {
    if(isset($_POST['id_serie'])) {
        $id_serie = $_POST['id_serie'];
        
        // Verificar si hay temporadas relacionadas
        $consulta_temporadas = "SELECT COUNT(*) AS total_temporadas FROM temporadas WHERE id_serie = '$id_serie'";
        $resultado_temporadas = mysqli_query($conn, $consulta_temporadas);
        $datos_temporadas = mysqli_fetch_assoc($resultado_temporadas);
        $total_temporadas = $datos_temporadas['total_temporadas'];
        
        // Verificar si hay capítulos relacionados
        $consulta_capitulos = "SELECT COUNT(*) AS total_capitulos FROM capitulos WHERE id_temporada IN (SELECT id_temporada FROM temporadas WHERE id_serie = '$id_serie')";
        $resultado_capitulos = mysqli_query($conn, $consulta_capitulos);
        $datos_capitulos = mysqli_fetch_assoc($resultado_capitulos);
        $total_capitulos = $datos_capitulos['total_capitulos'];
        
        if($total_temporadas > 0 || $total_capitulos > 0) {
            // Mostrar mensaje de advertencia
            $_SESSION['mensaje'] = "Esta serie tiene temporadas o capítulos asociados. Debes eliminar primero las temporadas y los capítulos.";
            header("Location: series_opciones.php");
            exit();
        } else {
            // Eliminar la serie
            $consulta_delete = "DELETE FROM series WHERE id_serie = '$id_serie'";
            $resultado_delete = mysqli_query($conn, $consulta_delete);
            if ($resultado_delete) {
                // Redireccionar a la página index de admin
                header("Location: index_admin.php");
                exit();
            } else {
                // Si hubo un error al borrar la serie, mostrar un mensaje de error
                $_SESSION['error_borrar'] = "Error al borrar la serie.";
                header("Location: series_opciones.php");
                exit();
            }
        }
    } else {
        // Mostrar un mensaje de error
        $_SESSION['error_borrar'] = "No se ha proporcionado el ID de la serie.";
        header("Location: series_opciones.php");
        exit();
    }
}
?>
