<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <title>Seleccionar documental</title>
    <link rel="stylesheet" href="css/style.css">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0/css/all.min.css" rel="stylesheet"/>
    <link rel="stylesheet" href="css/opciones.css">

</head>
<body>
    <h1>Selecciona un documental</h1>
    <form action="conf_documental.php" method="post" class="radio-form">
        <?php
        // Incluir el archivo de conexión a la base de datos
        // include('conexion.php');
        include 'conexionDebo.php';

        // Consultar la base de datos para obtener todos los documentales
        $sql = "SELECT id_documental, titulo FROM documentales";
        $result = mysqli_query($conn, $sql);

        // Verificar si hay documentales en la base de datos
        if ($result && mysqli_num_rows($result) > 0) {
            // Mostrar los documentales como opciones en un formulario
            while ($row = mysqli_fetch_assoc($result)) {
                echo "<div class='radio-option'>";
                echo "<input type='radio' id='documental{$row['id_documental']}' name='id_documental' value='{$row['id_documental']}'>";
                echo "<label for='documental{$row['id_documental']}'>{$row['titulo']}</label>";
                echo "</div>";
            }
        } else {
            echo "<p>No hay documentales disponibles.</p>";
        }

        // Cerrar la conexión a la base de datos
        mysqli_close($conn);
        ?>
        <input type="submit" value="Configurar documental" class="button radio-submit">
    </form>

    <br>
    <button><a href="añadir_documental.php">Añadir Documental</a></button>

    <h1>Eliminar Documental</h1>
    <form action="documentales_opciones.php" method="post" class="delete-form">
        <?php
        // Incluir el archivo de conexión a la base de datos
        // include('conexion.php');
        include 'conexionDebo.php';

        // Consultar la base de datos para obtener todas las películas
        $sql = "SELECT id_documental, titulo FROM documentales";
        $resultado = mysqli_query($conn, $sql);

        echo "<table border='1'>";
 
        while ($salida = mysqli_fetch_array($resultado)) {
            echo "<tr>";
            echo "<td><input type='radio' name='id_documental' value='{$salida['id_documental']}'></td>";
            echo "<td>{$salida['titulo']}</td>";
            echo "<td><form method='POST' action='fxDocumental.php'><input type='hidden' name='id_documental' value='{$salida['id_documental']}'><button type='submit' name='submit_borrar' value='borrar' class='button'>Borrar</button></form></td>";
            echo "</tr>";
        }
        
        echo "</table>";
 
        mysqli_close($conn);
        ?>
    </form>

   <button><a href="index_admin.php">Volver</a></button>
</body>
</html>
