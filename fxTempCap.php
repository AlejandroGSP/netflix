<?php

// Función para eliminar una temporada junto con sus capítulos asociados
function eliminarTemporada($id_temporada, $conn) {
    // Eliminar los capítulos asociados a la temporada
    $sql_eliminar_capitulos = "DELETE FROM capitulos WHERE id_temporada='$id_temporada'";
    $resultado_eliminar_capitulos = mysqli_query($conn, $sql_eliminar_capitulos);

    if (!$resultado_eliminar_capitulos) {
        // Manejar el error si falla la eliminación de los capítulos
        throw new Exception("Error al eliminar los capítulos asociados a la temporada: " . mysqli_error($conn));
    }

    // Luego, eliminar la temporada
    $sql_eliminar_temporada = "DELETE FROM temporadas WHERE id_temporada='$id_temporada'";
    $resultado_eliminar_temporada = mysqli_query($conn, $sql_eliminar_temporada);

    if (!$resultado_eliminar_temporada) {
        // Manejar el error si falla la eliminación de la temporada
        throw new Exception("Error al eliminar la temporada: " . mysqli_error($conn));
    }

    return true; // Devolver verdadero si la eliminación fue exitosa
}

// Función para eliminar un capítulo
function eliminarCapitulo($id_capitulo, $conn) {
    // Consulta para eliminar el capítulo
    $sql_eliminar_capitulo = "DELETE FROM capitulos WHERE id_capitulo='$id_capitulo'";
    $resultado_eliminar_capitulo = mysqli_query($conn, $sql_eliminar_capitulo);

    if (!$resultado_eliminar_capitulo) {
        // Manejar el error si falla la eliminación del capítulo
        throw new Exception("Error al eliminar el capítulo: " . mysqli_error($conn));
    }

    return true; // Devolver verdadero si la eliminación fue exitosa
}

?>
