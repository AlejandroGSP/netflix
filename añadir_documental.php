<?php
    session_start();
    // include 'conexion.php';
    include 'conexionDebo.php';
    
    // Verificar si se han enviado los datos del formulario
    if ($_SERVER["REQUEST_METHOD"] == "POST") {

        // Recuperar los datos del formulario
        $titulo = $_POST['titulo'];
        $descripcion = $_POST['descripcion'];
        $director = $_POST['director'];
        $año = $_POST['año_lanzamiento'];
        $duracion = $_POST['duracion_minutos'];
        $foto = $_POST['foto'];
        $video = $_POST['video'];

        // Preparar la consulta para insertar la película en la base de datos
        $sql = "INSERT INTO documentales (titulo, descripcion, director, año_lanzamiento, duracion_minutos, foto, video) VALUES ('$titulo', '$descripcion', '$director', $año, $duracion, '$foto', '$video')";

        // Ejecutar la consulta
        if (mysqli_query($conn, $sql)) {
            header("Location: index_admin.php");
            exit();
        } else {
            echo "<p>Error al añadir el documental: " . mysqli_error($conn) . "</p>";
        }


        // Cerrar la conexión a la base de datos
        mysqli_close($conn);
    }
    ?>

<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <title>Añadir Documental</title>
    <link rel="stylesheet" href="css/style.css">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0/css/all.min.css" rel="stylesheet"/>
    <link rel="stylesheet" href="css/añadir.css">
    <link rel="stylesheet" href="css\boton.css">
</head>
<body>
    <h1>Añadir Documental</h1>
    <form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method="post" enctype="multipart/form-data">
        <label for="titulo">Título:</label>
        <input type="text" id="titulo" name="titulo" required><br>
        
        <label for="descripcion">Descripción:</label><br>
        <textarea id="descripcion" name="descripcion" rows="4" cols="50" required></textarea><br>
        
        <label for="director">Director:</label>
        <input type="text" id="director" name="director" required><br>
        
        <label for="año_lanzamiento">Año de lanzamiento:</label>
        <input type="text" id="año_lanzamiento" name="año_lanzamiento" required><br>
        
        <label for="duracion_minutos">Duración (minutos):</label>
        <input type="text" id="duracion_minutos" name="duracion_minutos" required><br>
        
        <label for="foto">Foto de portada:</label>
        <input type="text" id="foto" name="foto"  ><br>
        
        <label for="video">Enlace del video:</label>
        <input type="text" id="video" name="video"><br>
        
        <input type="submit" value="Añadir Documental">
    </form>
    <button><a href="javascript:history.go(-1)">Volver</a></button>
    
</body>
</html>
