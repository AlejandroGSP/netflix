<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Formulario PHP</title>
</head>
<body>

<h2>Formulario PHP</h2>

<form method="post" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>">
    Nombre: <input type="text" name="nombre">
    <br><br>
    Email: <input type="text" name="email">
    <br><br>
    <input type="submit" name="submit" value="Enviar">
</form>

<?php
// Procesar el formulario cuando se envíe
if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $nombre = $_POST['nombre'];
    $email = $_POST['email'];

    // Hacer lo que quieras con los datos, por ejemplo, mostrarlos
    echo "<h2>Datos enviados:</h2>";
    echo "Nombre: " . $nombre . "<br>";
    echo "Email: " . $email;
}
?>

</body>
</html>