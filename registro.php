        <!DOCTYPE html>
        <html lang="en">
        <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Registro de Nuevo Usuario</title>
        <link rel="stylesheet" href="css\registro.css">
        </head>
        <body>
        
        <div class="container">
        <h2>Registro de Nuevo Usuario</h2>

        <form action="verificar_registro.php" method="post" class="registro-form">
        <label for="nombre">Nombre:</label>
        <input type="text" id="nombre" name="nombre" required><br><br>
        <label for="apellidos">Apellidos:</label>
        <input type="text" id="apellidos" name="apellidos" required><br><br>
        <label for="email">Email:</label>
        <input type="email" id="email" name="email" required><br><br>
        <label for="contraseña">Contraseña:</label>
        <input type="password" id="contraseña" name="contraseña" required><br><br>
        
                <h4>Selecciona tu foto:</h4>
        <div class="foto-options">
        <input type="radio" id="opcion1" name="fotoPerfil" value="usuarioHombre.jpg">   
        <label for="opcion1"><img src="img/usuarioHombre.jpg" alt="Foto Hombre"></label><br>
        <input type="radio" id="opcion2" name="fotoPerfil" value="usuarioMujer.jpg">
        <label for="opcion2"><img src="img/usuarioMujer.jpg" alt="Foto Mujer"></label><br>
        <input type="radio" id="opcion3" name="fotoPerfil" value="usuarioNino.jpg">
        <label for="opcion3"><img src="img/usuarioNino.jpg" alt="Foto Niño"></label><br>
        </div>
        
                <input type="submit" value="Registrar" class="submit-btn">
        </form>
        </div>
        
        </body>
        </html>