<?php
session_start();

//include 'conexion.php';
include 'conexionDebo.php';




if(isset($_POST['submit_borrar']) && $_POST['submit_borrar'] == "borrar") {
    // Suponiendo que la id de pelicula a borrar se pasa como parámetro en la URL
    if(isset($_POST['id_documental'])){
        $id_documental = $_POST['id_documental'];

        // Realizar la consulta SQL para borrar documental
        $consulta_delete = "DELETE FROM documentales WHERE id_documental = '$id_documental'";
        $resultado_delete = mysqli_query($conn, $consulta_delete);
        if ($resultado_delete) {
            // Redireccionar a la página index de admin
            header("Location: index_admin.php");
            exit();
        } else {
            // Si hubo un error al borrar documental, mostrar un mensaje de error
            $_SESSION['error_borrar'] = "Error al borrar documental.";
            header("Location: documentales_opciones.php");
            exit();
        }
    } else {
       //mostrar un mensaje de error
        $_SESSION['error_borrar'] = "No se ha proporcionado el id de documental.";
        header("Location: documentales_opciones.php");
        exit();
}
}
    



// // Cerrar la conexión
// mysqli_close($conexion);
// session_unset();
// session_destroy();
?>