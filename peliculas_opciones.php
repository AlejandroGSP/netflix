<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <title>Seleccionar película</title>
    <link rel="stylesheet" href="css/style.css">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0/css/all.min.css" rel="stylesheet"/>
    <link rel="stylesheet" href="css/opciones.css">
  
</head>
<body>
    <h1>Selecciona una película</h1>
    <form action="conf_psd.php" method="post" class="radio-form">
        <?php
        // Incluir el archivo de conexión a la base de datos
        // include('conexion.php');
        include 'conexionDebo.php';

        // Consultar la base de datos para obtener todas las películas
        $sql = "SELECT id_pelicula, titulo FROM peliculas";
        $result = mysqli_query($conn, $sql);

        // Verificar si hay películas en la base de datos
        if ($result && mysqli_num_rows($result) > 0) {
            // Mostrar las películas como opciones en un formulario
            while ($row = mysqli_fetch_assoc($result)) {
                echo "<div class='radio-option'>";
                echo "<input type='radio' id='pelicula{$row['id_pelicula']}' name='id_pelicula' value='{$row['id_pelicula']}'>";
                echo "<label for='pelicula{$row['id_pelicula']}'>{$row['titulo']}</label>";
                echo "</div>";
            }
        } else {
            echo "<p>No hay películas disponibles.</p>";
        }

        // Cerrar la conexión a la base de datos
        mysqli_close($conn);
        ?>
        <input type="submit" value="Configurar película" class="button radio-submit">
    </form>

    <!-- Botón para añadir película -->
    <button><a href="añadir_pelicula.php">Añadir Película</a></button>

    <h1>Eliminar película</h1>
    <form action="peliculas_opciones.php" method="post" class="delete-form">
        <?php
        // Incluir el archivo de conexión a la base de datos
        // include('conexion.php');
        include 'conexionDebo.php';

        // Consultar la base de datos para obtener todas las películas
        $sql = "SELECT id_pelicula, titulo FROM peliculas";
        $resultado = mysqli_query($conn, $sql);

        echo "<table border='1'>";
 
        while ($salida = mysqli_fetch_array($resultado)) {
            echo "<tr>";
            echo "<td><input type='radio' name='id_pelicula' value='{$salida['id_pelicula']}'></td>";
            echo "<td>{$salida['titulo']}</td>";
            echo "<td><form method='POST' action='fxPelicula.php'><input type='hidden' name='id_pelicula' value='{$salida['id_pelicula']}'><button type='submit' name='submit_borrar' value='borrar' class='button'>Borrar</button></form></td>";
            echo "</tr>";
        }
        
        echo "</table>";
 
        mysqli_close($conn);
        ?>
    </form>
     <button><a href="index_admin.php">Volver</a></button>
</body>
</html>
