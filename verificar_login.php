<?php
session_start();

//include 'conexion.php';
include 'conexionDebo.php';

 // Recibir datos del formulario
    $email = $_POST['email'];
    $contraseña = $_POST['contraseña'];

    $_SESSION['email'] = $email;

 // Verificar si el usuario existe en la base de datos y si la contraseña coincide
    $sql = "SELECT id_usuario, contraseña, rol FROM usuarios WHERE email='$email'";
    $result = $conn->query($sql);

    if ($result->num_rows > 0) {
        $row = $result->fetch_assoc();
        if (password_verify($contraseña, $row['contraseña'])) {
            $_SESSION['id_usuario'] = $row['id_usuario'];
            $_SESSION['rol'] = $row['rol'];
    
            if ($_SESSION['rol'] == '1') {
                header("Location: index_admin.php");
                exit();
            } else {
                header("Location: index_user.php");
                exit();
            }
        } else {
            $_SESSION['error_login'] = "Email o contraseña incorrectos";
            header("Location: login.php");
            exit();
        }
    } else {
        $_SESSION['error_login'] = "Email o contraseña incorrectos";
        header("Location: login.php");
        exit();
    }



$conn->close();
?>
