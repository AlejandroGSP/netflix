<?php
session_start();
// include 'conexion.php';
include 'conexionDebo.php';

// Obtener el ID de la serie enviado desde el formulario
$id_serie = isset($_POST['id_serie']) ? $_POST['id_serie'] : null;

$titulo = '';
$descripcion = '';
$creador = '';
$año_lanzamiento = '';
$foto = '';
$video = '';

// Si se ha enviado el ID de la serie, cargar los detalles de la serie y sus temporadas y capítulos
if ($id_serie) {
    // Consulta para obtener los detalles de la serie
    $sql_serie = "SELECT * FROM series WHERE id_serie='$id_serie'";
    $resultado_serie = mysqli_query($conn, $sql_serie);

    if ($resultado_serie && mysqli_num_rows($resultado_serie) > 0) {
        // Obtener los datos de la serie
        $serie = mysqli_fetch_assoc($resultado_serie);

        // Asignar los datos de la serie a variables
        $titulo = $serie['titulo'];
        $descripcion = $serie['descripcion'];
        $creador = $serie['creador'];
        $año_lanzamiento = $serie['año_lanzamiento'];
        $foto = $serie['foto'];
        $video = $serie['video'];
        
    } else {
        echo "Serie no encontrada";
    }
}

if ($_SERVER["REQUEST_METHOD"] == "POST") {
    if (isset($_POST['id_serie'], $_POST['titulo'], $_POST['descripcion'], $_POST['creador'], $_POST['año_lanzamiento'], $_POST['foto'], $_POST['video'])) {
        // Obtener los datos del formulario
        $nuevoTitulo = $_POST['titulo'];
        $nuevoDescripcion = $_POST['descripcion'];
        $nuevoCreador = $_POST['creador'];
        $nuevoAño_lanzamiento = $_POST['año_lanzamiento'];
        $nuevoFoto = $_POST['foto'];
        $nuevoVideo = $_POST['video'];
        $id_serie = $_POST['id_serie']; // Asegúrate de obtener el id_serie del formulario

        // Consulta para actualizar los datos de la serie
        $sql = "UPDATE series SET titulo='$nuevoTitulo', descripcion='$nuevoDescripcion', creador='$nuevoCreador', año_lanzamiento='$nuevoAño_lanzamiento', foto='$nuevoFoto', video='$nuevoVideo' WHERE id_serie='$id_serie'";

        $resultado = mysqli_query($conn, $sql);

        if ($resultado) {
            $_SESSION['mensaje'] = "Datos actualizados correctamente";
            // Redirigir a la página de índice del administrador después de guardar los cambios
            header("Location: index_admin.php");
            exit();
        } else {
            echo "Error al actualizar los datos: " . mysqli_error($conn);
        }
    } else {
        echo "No se han recibido todos los campos del formulario";
    }
}




?>

<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="css/series.css">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0/css/all.min.css" rel="stylesheet"/>
    <!-- <link rel="stylesheet" href="css\boton.css"> -->
    <title>Modificar Serie</title>
</head>
<body>
<?php
// Mostrar mensaje de éxito si existe
if (isset($_SESSION['mensaje'])) {
    echo "<p>{$_SESSION['mensaje']}</p>";
    unset($_SESSION['mensaje']); // Limpiar el mensaje para evitar que se muestre de nuevo en futuras recargas de la página
}
?>
    <h1>Modificar Serie</h1>
    <!-- Formulario para modificar los detalles de la serie -->
    <form method="post" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>">
        <!-- Campos para modificar los detalles de la serie -->
        <label for="titulo">Título: </label>
        <input type="text" id="titulo" name="titulo" value="<?php echo isset($titulo) ? $titulo : ''; ?>"><br><br>

        <label for="descripcion">Descripción:</label>
        <input type="text" id="descripcion" name="descripcion"  value="<?php echo isset($descripcion) ? $descripcion : ''; ?>"><br><br>

        <label for="creador">Creador: </label>
        <input type="text" id="creador" name="creador" value="<?php echo isset($creador) ? $creador : ''; ?>"><br><br>

        <label for="año_lanzamiento">Año de Lanzamiento: </label>
        <input type="text" id="año_lanzamiento" name="año_lanzamiento" value="<?php echo isset($año_lanzamiento) ? $año_lanzamiento : ''; ?>"><br><br>

        <label for="foto">Foto (URL): </label>
        <input type="text" id="foto" name="foto" value="<?php echo isset($foto) ? $foto : ''; ?>"><br><br>

        <label for="video">Video (URL): </label>
        <input type="text" id="video" name="video" value="<?php echo isset($video) ? $video : ''; ?>"><br><br>

        <!-- Botón para guardar los cambios -->
        <input type="submit" value="Guardar Cambios">
        <!-- Campo oculto para enviar el ID de la serie -->
        <input type="hidden" name="id_serie" value="<?php echo $id_serie; ?>">
        <br>
        <br>
    </form>

    <!-- Botón para ir a la configuración de temporadas y capítulos -->
    <form method="post" action="selec_temp.php">
        <!-- Campo oculto para enviar el ID de la serie -->
        <input type="hidden" name="id_serie" value="<?php echo $id_serie; ?>">
        <input type="submit" value="Configurar Temporadas y Capítulos">
        <br>
        <br>
    </form>

    <form method="post" action="añadir_temporada.php">
    <!-- Campo oculto para enviar el ID de la serie -->
    <input type="hidden" name="id_serie" value="<?php echo isset($_POST['id_serie']) ? $_POST['id_serie'] : ''; ?>">
    <input type="submit" value="Añadir Temporada">
    <br>
    <br>
</form>


<button><a href="javascript:history.go(-1)">Volver</a></button>

</body>
</html>
