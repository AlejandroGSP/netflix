<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="css\style.css">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0/css/all.min.css" rel="stylesheet"/>
    <title>Página de Inicio</title>     
</head>
<body>
    
    <header>
        <nav class="contenedor_nav">
            <div class="marca">
                <h1><span>CHILE</span>FLIX</h1>
            </div>
            <ul class="menu">
                <button class="button">
                    <a href="login.php">Iniciar Sesión</a>
                </button>
            </ul> 
        </nav>
    </header>

    <main>

        <div class="inicio">
            <div class="inicio__eslogan">
                <h2>Tu entretenimiento, tu elección: ¡Descubre un mundo de películas y series en nuestra plataforma!</h2>
            </div>

            <div class="inicio__img">
                <div class="cartelera-container ">
                    <div class="cartelera" style="background-image:url('./img/Vengadores.jpg');"></div>
                    <div class="cartelera"  style="background-image:url('./img/TWD.jpg');"></div>
                    <div class="cartelera"  style="background-image:url('./img/Last_Dance.jpg');"></div>
                    <div class="cartelera"  style="background-image:url('./img/Jumanji.jpg');"></div>
                    <div class="cartelera"  style="background-image:url('./img/Peaky_Blinders.jpg');"></div>
                    <div class="cartelera"  style="background-image:url('./img/LVENP.jpg');"></div>
                    <div class="cartelera"  style="background-image:url('./img/Deadpool.jpg');"></div>
                    <div class="cartelera"  style="background-image:url('./img/Kimetsu.png');"></div>
                    <div class="cartelera"  style="background-image:url('./img/Alcasser.jpg');"></div>
                    <div class="cartelera"  style="background-image:url('./img/Dos_Rubias.png');"></div>
                    <div class="cartelera"  style="background-image:url('./img/HIMYM.jpg');"></div>
                    <div class="cartelera"  style="background-image:url('./img/DEPP.jpg');"></div>
                    <div class="cartelera"  style="background-image:url('./img/La_Monja.jpg');"></div>
                    <div class="cartelera"  style="background-image:url('./img/Marianne.png');"></div>
                    <div class="cartelera"  style="background-image:url('./img/MSR.jpg');"></div>
                    <div class="cartelera"  style="background-image:url('./img/Shrek.jpg');"></div>
                    <div class="cartelera"  style="background-image:url('./img/Bob_Esponja.jpg');"></div>
                    <div class="cartelera"  style="background-image:url('./img/Georgina.jpg');"></div>
                </div>                                  
            </div>

            <div class="inicio__descripcion">
                <h3>Nuestra plataforma ofrece una experiencia de entretenimiento incomparable, donde tú tienes el control total.
                Con acceso a una amplia gama de películas y series, te invitamos a sumergirte en un universo de emocionantes historias
                y contenido exclusivo. Desde éxitos de taquilla hasta joyas independientes, nuestro catálogo está diseñado para satisfacer
                todos los gustos y preferencias. Además, con funciones personalizadas y recomendaciones adaptadas a tus intereses, cada sesión
                de visualización se convierte en una experiencia única y envolvente.</h3>
            </div>

        </div>
    </main>

    <footer class="footer">

        <div class="main">
            <div class="footer__content">
                    <h2><span>CHILE</span>FLIX</h2>
                    <h5>En <span>CHILEFLIX</span> somos amantes del mundo audiovisual y desde 2024 dedicamos nuestro día a día a traerte las mejores peliculas, series y documentales.
                        Te ofrecemos las últimas novedades para que disfrutes de tu plataforma favorita.</h5>
                    <h5>contacto<span>@chileflix.com</span></h5>
                    <h5>+56<span> 991234780</span></h5>
                </div>

                <div class="footer__socialnetwork">
                    <ul>
                        <li>
                            <a href="#" class="twitter">
                                <i class="fab fa-twitter fa-2x"></i>
                            </a>
                        </li>
                        <li>
                            <a href="#" class="instagram">
                                <i class="fab fa-instagram fa-2x"></i>
                            </a>
                        </li>                    
                        <li>
                            <a href="#" class="youtube">
                                <i class="fab fa-youtube fa-2x"></i>
                            </a>
                        </li>
                        <li>
                            <a href="#" class="google">
                                <i class="fab fa-google fa-2x"></i>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>      
    </footer>

</body>
</html>