<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="css\style.css">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0/css/all.min.css" rel="stylesheet"/>
    <title>Página de Peliculas</title>
</head>
<body>
    <?php
    session_start();

    //include 'conexion.php';
    include 'conexionDebo.php';

  

    //consulta como tal 
    $sql = "SELECT * FROM peliculas ";
    $resultado = mysqli_query($conn, $sql);
    //    $consulta->bindParam(':email', $email);
    

    if ($resultado && mysqli_num_rows($resultado) > 0) {
        // Obtener los datos del usuario
        $peliculas = mysqli_fetch_assoc($resultado);


        // Datos de la pelicula
        $titulo = $peliculas['titulo'];
        $director = $peliculas['director'];
        $descripcion = $peliculas['descripcion'];
        $duracion = $peliculas['duracion_minutos'];
        $video = $peliculas['video'];
    }
   
    ?>
    <a href="javascript:history.go(-1)" class="botonparatodo">Inicio</a>

    <div class="peliculas">
        <div class="peliculas__titulo">
            <?php echo "<h1>$titulo</h1>";?>
        </div>
        <div class="peliculas__director">
            <?php echo "<p>Director: $director</p>";?>
        </div>
        <div class="peliculas__descripcion">
            <?php echo "<p>Descripción: $descripcion</p>";?>
        </div>
        <div class="peliculas__duracion">
            <?php echo "<p>Duración: $duracion</p>";?>
        </div>
        <div class="peliculas__video">
            <?php echo "<p>$video</p>";?>
        </div>
    </div>

    <?php
    $session_close;
      ?>

    
</body>
</html>