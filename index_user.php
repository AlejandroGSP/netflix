<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="css\style.css">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0/css/all.min.css" rel="stylesheet"/>
    <title>Página de Indice Usuario</title>
</head>
<body>

    <header>

        <nav class="contenedor_nav" id="inicio">
            <div class="marca">
                <h1><span>CHILE</span>FLIX</h1>
            </div>
            <ul class="menu">
                <li><a href="#inicio">INICIO</a></li>
                <li><a href="#peliculas">PELICULAS</a></li>
                <li><a href="#series">SERIES</a></li>
                <li><a href="#documentales">DOCUMENTALES</a></li>
            </ul>
            <ul class="conexion">
                <li><a href="mi_perfil.php">Perfil</a>
                    <ul class="admin">
                        <li><a href="modificarDatos.php">Gestionar Perfil</a></li>
                        <li><a href="cerrarSesion.php">Cerrar Sesión</a></li>
                    </ul>
                </li>
            </ul>
        </nav>

    </header>

    <main>

        <div class="seccion">
            <div class="grupo">
                <div class="grupo__titulo">
                    <h2 id="peliculas">PELÍCULAS</h2>
                </div>
                <div class="grupo__img">
                    <a href="peliculas.php"><div <?php if(isset($fotopeli) && $fotopeli == './img/Deadpool.jpg') echo 'checked'; ?> class="producto"  style="background-image:url('./img/Deadpool.jpg');"></div></a>
                    <a href="peliculas.php"><div <?php if(isset($fotopeli) && $fotopeli == './img/Dos_Rubias.jpg') echo 'checked'; ?>  class="producto"  style="background-image:url('./img/Dos_Rubias.jpg');"></div></a>
                    <a href="peliculas.php"><div <?php if(isset($fotopeli) && $fotopeli == './img/Jumanji.jpg') echo 'checked'; ?>  class="producto"  style="background-image:url('./img/Jumanji.jpg');"></div></a>
                    <a href="peliculas.php"><div <?php if(isset($fotopeli) && $fotopeli == './imgLa_Monja.jpg') echo 'checked'; ?>  class="producto"  style="background-image:url('./img/La_Monja.jpg');"></div></a>
                    <a href="peliculas.php"><div <?php if(isset($fotopeli) && $fotopeli == './img/Vengadores.jpg') echo 'checked'; ?>  class="producto" style="background-image:url('./img/Vengadores.jpg');"></div></a>
                    <a href="peliculas.php"><div <?php if(isset($fotopeli) && $fotopeli == './img/Shrek.jpg') echo 'checked'; ?>  class="producto"  style="background-image:url('./img/Shrek.jpg');"></div></a>
                </div>
                </div>
            </div>

            <div class="grupo">
                <div class="grupo__titulo">
                    <h2 id="series">SERIES</h2>
                </div>
                <div class="grupo__img">
                    <div class="producto"  style="background-image:url('./img/Bob_Esponja.jpg');"></div>
                    <div class="producto"  style="background-image:url('./img/HIMYM.jpg');"></div>
                    <div class="producto"  style="background-image:url('./img/Kimetsu.jpg');"></div>
                    <div class="producto"  style="background-image:url('./img/Marianne.jpg');"></div>
                    <div class="producto"  style="background-image:url('./img/Peaky_Blinders.jpg');"></div>
                    <div class="producto"  style="background-image:url('./img/TWD.jpg');"></div>
                </div>
            </div>

            <div class="grupo">
                <div class="grupo__titulo">
                    <h2 id="documentales">DOCUMENTALES</h2>
                </div>
                <div class="grupo__img">
                <a href="documentales.php"><div <?php if(isset($fotodocumental) && $fotodocumental == './img/Last_Dance.jpg') echo 'checked'; ?>class="producto"  style="background-image:url('./img/Last_Dance.jpg');"></div></a>
                <a href="documentales.php"><div <?php if(isset($fotodocumental) && $fotodocumental == './img/LVENP.jpg') echo 'checked'; ?>class="producto"  style="background-image:url('./img/LVENP.jpg');"></div></a>
                <a href="documentales.php"><div <?php if(isset($fotodocumental) && $fotodocumental == './img/Alcasser.jpg') echo 'checked'; ?>class="producto"  style="background-image:url('./img/Alcasser.jpg');"></div></a>
                <a href="documentales.php"><div <?php if(isset($fotodocumental) && $fotodocumental == './img/DEPP.jpg') echo 'checked'; ?>class="producto"  style="background-image:url('./img/DEPP.jpg');"></div></a>
                <a href="documentales.php"><div <?php if(isset($fotodocumental) && $fotodocumental == './img/MSR.jpg') echo 'checked'; ?>class="producto"  style="background-image:url('./img/MSR.jpg');"></div></a>
                <a href="documentales.php"><div <?php if(isset($fotodocumental) && $fotodocumental == './img/Georgina.jpg') echo 'checked'; ?>class="producto"  style="background-image:url('./img/Georgina.jpg');"></div></a>
                </div>
            </div>
        </div>

    </main>

    <footer class="footer">

        <div class="main">
            <div class="footer__content">
                    <h2><span>CHILE</span>FLIX</h2>
                    <h5>En <span>CHILEFLIX</span> somos amantes del mundo audiovisual y desde 2024 dedicamos nuestro día a día a traerte las mejores peliculas, series y documentales.
                        Te ofrecemos las últimas novedades para que disfrutes de tu plataforma favorita.</h5>
                    <h5>contacto<span>@chileflix.com</span></h5>
                    <h5>+56<span> 991234780</span></h5>
                </div>

                <div class="footer__socialnetwork">
                    <ul>
                        <li>
                            <a href="#" class="twitter">
                                <i class="fab fa-twitter fa-2x"></i>
                            </a>
                        </li>
                        <li>
                            <a href="#" class="instagram">
                                <i class="fab fa-instagram fa-2x"></i>
                            </a>
                        </li>                    
                        <li>
                            <a href="#" class="youtube">
                                <i class="fab fa-youtube fa-2x"></i>
                            </a>
                        </li>
                        <li>
                            <a href="#" class="google">
                                <i class="fab fa-google fa-2x"></i>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>      
         
    </footer>
    
</body>
</html>