<?php
session_start();

// include 'conexion.php';
include 'conexionDebo.php';

// Obtener el ID de la película enviado desde el formulario
$id_pelicula = isset($_POST['id_pelicula']) ? $_POST['id_pelicula'] : null;

// Variables para almacenar los datos de la película
$titulo = '';
$descripcion = '';
$director = '';
$año_lanzamiento = '';
$duracion_minutos = '';
$foto = '';
$video = '';

// Si se ha enviado el ID de la película, consultar y cargar los datos de la película
if ($id_pelicula) {
    $sql = "SELECT * FROM peliculas WHERE id_pelicula='$id_pelicula'";
    $resultado = mysqli_query($conn, $sql);

    if ($resultado && mysqli_num_rows($resultado) > 0) {
        // Obtener los datos de la película
        $pelicula = mysqli_fetch_assoc($resultado);

        // Asignar los datos de la película a las variables correspondientes
        $titulo = $pelicula['titulo'];
        $descripcion = $pelicula['descripcion'];
        $director = $pelicula['director'];
        $año_lanzamiento = $pelicula['año_lanzamiento'];
        $duracion_minutos = $pelicula['duracion_minutos'];
        $foto = $pelicula['foto'];
        $video = $pelicula['video'];
    } else {
        echo "Pelicula no encontrada";
    }
}

// ACTUALIZACIÓN DE LOS DATOS
if ($_SERVER["REQUEST_METHOD"] == "POST" && isset($_POST['id_pelicula'])) {
    // Verificar si se han recibido todos los campos del formulario
    if (isset($_POST['titulo'], $_POST['descripcion'], $_POST['director'], $_POST['año_lanzamiento'], $_POST['duracion_minutos'], $_POST['foto'], $_POST['video'])) {
        // Obtener los datos del formulario
        $nuevoTitulo = $_POST['titulo'];
        $nuevoDescripcion = $_POST['descripcion'];
        $nuevoDirector = $_POST['director'];
        $nuevoAño_lanzamiento = $_POST['año_lanzamiento'];
        $nuevoDuracion_minutos = $_POST['duracion_minutos'];
        $nuevoFoto = $_POST['foto'];
        $nuevoVideo = $_POST['video'];

        // Consulta para actualizar los datos de la película
        $sql = "UPDATE peliculas SET titulo='$nuevoTitulo', descripcion='$nuevoDescripcion', director='$nuevoDirector', año_lanzamiento='$nuevoAño_lanzamiento', duracion_minutos='$nuevoDuracion_minutos', foto='$nuevoFoto', video='$nuevoVideo' WHERE id_pelicula='$id_pelicula'";

        $resultado = mysqli_query($conn, $sql);

        if ($resultado) {
            $_SESSION['mensaje'] = "Datos actualizados correctamente";
            // Redirigir a la página de índice del administrador después de guardar los cambios
            header("Location: index_admin.php");
            exit(); // Asegurar que el script se detenga después de la redirección
        } else {
            echo "Error al actualizar los datos: " . mysqli_error($conn);
        }
    } else {
        echo "No se han recibido todos los campos del formulario";
    }
}
?>

<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="css\modificardatos.css">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0/css/all.min.css" rel="stylesheet"/>
    <link rel="stylesheet" href="css\boton.css">
    <title>Página de Índice Admin</title>
</head>
<body>

<?php
// Mostrar mensaje de éxito si existe
if (isset($_SESSION['mensaje'])) {
    echo "<p>{$_SESSION['mensaje']}</p>";
    unset($_SESSION['mensaje']); // Limpiar el mensaje para evitar que se muestre de nuevo en futuras recargas de la página
}
?>

<div class="form-container">
    <h2>Modificar Datos de Película</h2>
    <form method="post" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>">

        <label for="titulo">Título: </label>
        <input type="text" id="titulo" name="titulo" value="<?php echo $titulo; ?>"><br><br>

        <label for="descripcion">Descripción:</label>
        <input type="text" id="descripcion" name="descripcion"  value="<?php echo $descripcion; ?>"><br><br>

        <label for="director">Director: </label>
        <input type="text" id="director" name="director" value="<?php echo $director; ?>"><br><br>

        <label for="año_lanzamiento">Año de Lanzamiento: </label>
        <input type="text" id="año_lanzamiento" name="año_lanzamiento" value="<?php echo $año_lanzamiento; ?>"><br><br>

        <label for="duracion_minutos">Duración:</label>
        <input type="text" id="duracion_minutos" name="duracion_minutos" value="<?php echo $duracion_minutos; ?>"><br><br>

        <label for="foto">Foto (URL): </label>
        <input type="text" id="foto" name="foto" value="<?php echo $foto; ?>"><br><br>

        <label for="video">Video (URL): </label>
        <input type="text" id="video" name="video" value="<?php echo $video; ?>"><br><br>

        <input type="hidden" name="id_pelicula" value="<?php echo $id_pelicula; ?>"> <!-- Agregar un campo oculto para enviar el ID de la película -->

        <input type="submit" value="Guardar Cambios">

    </form>
</div>
<button><a href="javascript:history.go(-1)">Volver</a></button>
</body>
</html>
