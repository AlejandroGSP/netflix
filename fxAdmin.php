<?php
session_start();

//include 'conexion.php';
include 'conexionDebo.php';
global $conn;

if(isset($_POST['submit_permisos']) && $_POST['submit_permisos'] == "permisos") {
    
    // Verificar si se recibió un ID de usuario válido
if(isset($_POST['id_usuario']) && !empty($_POST['id_usuario'])) {
    // Validar y sanear el ID del usuario recibido
    $idUsuario = mysqli_real_escape_string($conn, $_POST['id_usuario']);

    // Realizar la consulta SQL para actualizar el rol del usuario a administrador
    $consulta = "UPDATE usuarios SET rol = 1 WHERE id_usuario = '$idUsuario'";

    // Ejecutar la consulta
    if(mysqli_query($conn, $consulta)) {
        // Redireccionar a la página anterior con un mensaje de éxito
        header("Location: funcion_admin.php");
        exit();
        echo "Cambios realizados con éxito";
    } else {
        // Redireccionar a la página anterior con un mensaje de error si la consulta falla
        header("Location: funcion_admin.php");
        exit();
        echo "No se han podido realizar los cambios";
    } 
}else {
    // Redireccionar a la página anterior si no se proporciona un ID de usuario válido
    echo "No existe el usuario";
    header("Location: funcion_admin.php");
    exit();
}
};

if(isset($_POST['submit_borrar']) && $_POST['submit_borrar'] == "borrar") {
    // Suponiendo que el nombre de usuario a borrar se pasa como parámetro en la URL
    if(isset($_POST['id_usuario'])){
        $nombreUsuario = $_POST['id_usuario'];

        // Realizar la consulta SQL para borrar el perfil del usuario
        $consulta_delete = "DELETE FROM usuarios WHERE id_usuario = '$nombreUsuario'";
        $resultado_delete = mysqli_query($conn, $consulta_delete);
        if ($resultado_delete) {
            // Redireccionar a la página de administrador después de borrar el perfil
            header("Location: funcion_admin.php");
            exit();
        } else {
            // Si hubo un error al borrar el perfil, mostrar un mensaje de error
            $_SESSION['error_borrar'] = "Error al borrar el perfil del usuario.";
            header("Location: funcion_admin.php");
            exit();
        }
    } else {
        // Si no se proporciona el nombre de usuario a borrar, mostrar un mensaje de error
        $_SESSION['error_borrar'] = "No se ha proporcionado el nombre de usuario a borrar.";
        header("Location: funcion_admin.php");
        exit();
}
}
    



// // Cerrar la conexión
// mysqli_close($conexion);
// session_unset();
// session_destroy();
?>