<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="css\style.css">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0/css/all.min.css" rel="stylesheet"/>
    <title>Página de Documentales</title>
</head>
<body>
    <?php
    session_start();

    //include 'conexion.php';
    include 'conexionDebo.php';

  
    //consulta como tal

$sql = "SELECT * FROM documentales";
$resultado = mysqli_query($conn, $sql);
// -- $consulta->bindParam(':email', $email);

if ($resultado && mysqli_num_rows($resultado) > 0) {
// Obtener los datos de la pelicula
$documentales = mysqli_fetch_assoc($resultado);

// Datos de documentales
$titulo = $documentales['titulo'];
$director = $documentales['director'];
$descripcion = $documentales['descripcion'];
$añoLanzamiento = $documentales['año_lanzamiento'];
$duracion = $documentales['duracion_minutos'];
$video = $documentales['video'];

}
   ?>
   
    
   <a href="javascript:history.go(-1)" class="botonparatodo">Inicio</a>

    <div class="peliculas">
        <div class="peliculas__titulo">
            <?php echo "<h1>$titulo</h1>";?>
        </div>
        <div class="peliculas__director">
            <?php echo "<p>Director: $director</p>";?>
        </div>
        <div class="peliculas__descripcion">
            <?php echo "<p>Descripción: $descripcion</p>";?>
        </div>
        <div class="peliculas__anioLanzamiento">
            <?php echo "<p>Año Lanzamiento: $añoLanzamiento</p>";?>
        </div>
        <div class="peliculas__duracion">
            <?php echo "<p>Duración: $duracion</p>";?>
        </div>
        <div class="peliculas__video">
            <?php echo "<p>$video</p>";?>
        </div>
    </div>

    <?php
    $session_close;
      ?>

    
</body>
</html>