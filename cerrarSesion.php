<?php
session_start();

// Cerrar la sesión
session_unset();
session_destroy();

// Cerrar la conexión a la base de datos si es necesario
if (isset($conn)) {
    $conn->close();
}

// Redireccionar al usuario a la página de inicio
header("Location: pagina_inicio.php");
exit();
?>