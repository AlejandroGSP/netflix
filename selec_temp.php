<?php 
session_start(); 
include 'conexionDebo.php';
//include 'conexion.php';
include 'fxTempCap.php'; // Incluir el archivo con la función eliminarTemporada

// Obtener el ID de la serie enviado desde el formulario
$id_serie = isset($_POST['id_serie']) ? $_POST['id_serie'] : null;

// Consulta para obtener las temporadas de la serie
$sql_temporadas = "SELECT * FROM temporadas WHERE id_serie='$id_serie'";
$resultado_temporadas = mysqli_query($conn, $sql_temporadas);

// Array para almacenar las temporadas disponibles
$temporadas = [];

if ($resultado_temporadas && mysqli_num_rows($resultado_temporadas) > 0) {
    while ($temporada = mysqli_fetch_assoc($resultado_temporadas)) {
        $temporadas[] = $temporada;
    }
}

// Verificar si se ha enviado una solicitud para eliminar una temporada
if ($_SERVER["REQUEST_METHOD"] == "POST" && isset($_POST['eliminar_temporada'])) {
    $id_temporada = $_POST['eliminar_temporada'];
    
    // Llamar a la función para eliminar la temporada
    $eliminacion_exitosa = eliminarTemporada($id_temporada, $conn);
    
    if ($eliminacion_exitosa) {
        $_SESSION['mensaje'] = "Temporada eliminada correctamente";
    } else {
        $_SESSION['mensaje'] = "Error al eliminar la temporada";
    }
    
    // Redirigir a la misma página para evitar envíos repetidos del formulario
    header("Location: selec_temp.php?id_serie=$id_serie");
    exit();
}
?>

<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Seleccionar Temporada</title>
    <link rel="stylesheet" href="css/style.css">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0/css/all.min.css" rel="stylesheet"/>
    <link rel="stylesheet" href="css/opciones.css">
   
</head>
<body>
    <h1>Seleccionar Temporada</h1>
    <form method="post" action="conf_cap.php">
        <label for="temporada">Selecciona una temporada:</label>
        <select name="id_temporada" id="temporada">
            <?php foreach ($temporadas as $temporada): ?>
                <option value="<?php echo $temporada['id_temporada']; ?>"><?php echo $temporada['numero_temporada']; ?></option>
            <?php endforeach; ?>
        </select>
        <input type="hidden" name="id_serie" value="<?php echo $id_serie; ?>">
        <input type="submit" value="Seleccionar">
    </form>
    
    <!-- Mostrar mensaje de éxito o error -->
    <?php if (isset($_SESSION['mensaje'])): ?>
        <p><?php echo $_SESSION['mensaje']; ?></p>
        <?php unset($_SESSION['mensaje']); ?>
    <?php endif; ?>
    
    <!-- Mostrar tabla de temporadas -->
    <table>
        <thead>
            <tr>
                <th>Número de Temporada</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($temporadas as $temporada): ?>
                <tr>
                    <td><?php echo $temporada['numero_temporada']; ?></td>
                    <td>
                        <form method="post" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>">
                            <input type="hidden" name="eliminar_temporada" value="<?php echo $temporada['id_temporada']; ?>">
                            <input type="hidden" name="id_serie" value="<?php echo $id_serie; ?>">
                            <button type="submit">Eliminar</button>
                        </form>
                    </td>
                </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <button><a href="series_opciones.php">Volver</a></button>
</body>
</html>
