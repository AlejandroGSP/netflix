<?php
session_start();

//include 'conexion.php';
include 'conexionDebo.php';




if(isset($_POST['submit_borrar']) && $_POST['submit_borrar'] == "borrar") {
    // Suponiendo que la id de pelicula a borrar se pasa como parámetro en la URL
    if(isset($_POST['id_pelicula'])){
        $id_pelicula = $_POST['id_pelicula'];

        // Realizar la consulta SQL para borrar la pelicula
        $consulta_delete = "DELETE FROM peliculas WHERE id_pelicula = '$id_pelicula'";
        $resultado_delete = mysqli_query($conn, $consulta_delete);
        if ($resultado_delete) {
            // Redireccionar a la página index de admin
            header("Location: index_admin.php");
            exit();
        } else {
            // Si hubo un error al borrar la pelicula, mostrar un mensaje de error
            $_SESSION['error_borrar'] = "Error al borrar la pelicula.";
            header("Location: peliculas_opciones.php");
            exit();
        }
    } else {
       //mostrar un mensaje de error
        $_SESSION['error_borrar'] = "No se ha proporcionado el id de pelicula.";
        header("Location: peliculas_opciones.php");
        exit();
}
}
    



// // Cerrar la conexión
// mysqli_close($conexion);
// session_unset();
// session_destroy();
?>