<?php
session_start();
// include 'conexion.php';
include 'conexionDebo.php';

// Verificar si se ha enviado el formulario para agregar un nuevo capítulo
if ($_SERVER["REQUEST_METHOD"] == "POST" && isset($_POST['id_temporada'])) {
    // Recuperar los datos del formulario
    $id_temporada = $_POST['id_temporada'];
    $titulo = $_POST['titulo'];
    $descripcion = $_POST['descripcion'];
    $numero_capitulo = $_POST['numero_capitulo'];

    // Insertar el nuevo capítulo en la base de datos
    $sql = "INSERT INTO capitulos (id_temporada, titulo, descripcion, numero_capitulo) VALUES ('$id_temporada', '$titulo', '$descripcion', '$numero_capitulo')";
    $resultado = mysqli_query($conn, $sql);

    if ($resultado) {
        $_SESSION['mensaje'] = "Capítulo agregado correctamente";
        header("Location: series_opciones.php"); // Redirigir de vuelta a la página de configuración de la temporada
        exit();
    } else {
        echo "Error al agregar el capítulo: " . mysqli_error($conn);
    }
}
?>

<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <title>Añadir Capítulo</title>
    <link rel="stylesheet" href="css/style.css">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0/css/all.min.css" rel="stylesheet"/>
    <link rel="stylesheet" href="css/series.css">
    <link rel="stylesheet" href="css/boton.css">
</head>
<body>
    <h1>Añadir Capítulo</h1>
    <!-- Formulario para agregar un nuevo capítulo -->
    <form method="post" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>">
        <label for="titulo">Título del Capítulo:</label>
        <input type="text" id="titulo" name="titulo" required><br><br>
        
        <label for="descripcion">Descripción del Capítulo:</label>
        <input type="text" id="descripcion" name="descripcion" required><br><br>

        <label for="numero_capitulo">Número de Capítulo:</label>
        <input type="text" id="numero_capitulo" name="numero_capitulo" required><br><br>

        <!-- Campo oculto para enviar el ID de la temporada -->
        <input type="hidden" name="id_temporada" value="<?php echo $_GET['id_temporada']; ?>">

        <!-- Botón para agregar el capítulo -->
        <input type="submit" value="Agregar Capítulo">
    </form>
    <button><a href="series_opciones.php">Volver</a></button>
</body>
</html>
