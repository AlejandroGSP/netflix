<?php
session_start();
include 'conexionDebo.php';
include 'fxTempCap.php'; // Incluir el archivo con la función eliminarCapitulo

// Obtener el ID de la serie enviado desde el formulario
$id_serie = isset($_POST['id_serie']) ? $_POST['id_serie'] : null;

// Array para almacenar las temporadas disponibles
$temporadas = [];

// Si se ha enviado el ID de la serie, cargar las temporadas disponibles
if ($id_serie) {
    // Consulta para obtener las temporadas de la serie
    $sql_temporadas = "SELECT * FROM temporadas WHERE id_serie='$id_serie'";
    $resultado_temporadas = mysqli_query($conn, $sql_temporadas);

    if ($resultado_temporadas && mysqli_num_rows($resultado_temporadas) > 0) {
        while ($temporada = mysqli_fetch_assoc($resultado_temporadas)) {
            $temporadas[] = $temporada;
        }
    }
}

// Si se ha enviado la solicitud para eliminar una temporada
if ($_SERVER["REQUEST_METHOD"] == "POST" && isset($_POST['eliminar_temporada'])) {
    $id_temporada = $_POST['eliminar_temporada'];
    
    // Llamar a la función para eliminar la temporada
    $eliminacion_exitosa = eliminarTemporada($id_temporada, $conn);
    
    if ($eliminacion_exitosa) {
        $_SESSION['mensaje'] = "Temporada eliminada correctamente";
    } else {
        $_SESSION['mensaje'] = "Error al eliminar la temporada";
    }
    
    // Redirigir a la misma página para evitar envíos repetidos del formulario
    header("Location: series_opciones.php ");
    exit();
}

// Si se ha enviado el ID de la temporada, cargar los detalles de los capítulos
$id_temporada = isset($_POST['id_temporada']) ? $_POST['id_temporada'] : null;
$capitulos = [];

if ($id_temporada) {
    // Consulta para obtener los capítulos de la temporada
    $sql_capitulos = "SELECT * FROM capitulos WHERE id_temporada='$id_temporada'";
    $resultado_capitulos = mysqli_query($conn, $sql_capitulos);

    if ($resultado_capitulos && mysqli_num_rows($resultado_capitulos) > 0) {
        while ($capitulo = mysqli_fetch_assoc($resultado_capitulos)) {
            $capitulos[] = $capitulo;
        }
    }
}

// Si se ha enviado la solicitud para eliminar un capítulo
if ($_SERVER["REQUEST_METHOD"] == "POST" && isset($_POST['eliminar_capitulo'], $_POST['id_temporada'])) {
    $id_capitulo_a_eliminar = $_POST['eliminar_capitulo'];
    $id_temporada = $_POST['id_temporada'];
    
    try {
        // Llamar a la función eliminarCapitulo
        eliminarCapitulo($id_capitulo_a_eliminar, $conn);
        
        // Redirigir de nuevo a esta página para refrescar los datos
        header("Location: series_opciones.php ");
        exit();
    } catch (Exception $e) {
        echo "Error: " . $e->getMessage();
    }
}

// Si se ha enviado la solicitud para actualizar los datos de un capítulo
if ($_SERVER["REQUEST_METHOD"] == "POST" && isset($_POST['capitulos'])) {
    // Verificar si se han enviado los datos para modificar los capítulos
    $capitulos_actualizados = $_POST['capitulos'];
    
    foreach ($capitulos_actualizados as $id_capitulo => $capitulo_actualizado) {
        // Obtener los datos del capítulo actualizado
        $nuevo_titulo = $capitulo_actualizado['titulo'];
        $nueva_descripcion = $capitulo_actualizado['descripcion'];
        $nuevo_numero_capitulo = $capitulo_actualizado['numero_capitulo'];

        // Consulta para actualizar los datos del capítulo
        $sql_actualizar_capitulo = "UPDATE capitulos SET titulo='$nuevo_titulo', descripcion='$nueva_descripcion', numero_capitulo='$nuevo_numero_capitulo' WHERE id_capitulo='$id_capitulo'";
        $resultado_actualizar_capitulo = mysqli_query($conn, $sql_actualizar_capitulo);

        if (!$resultado_actualizar_capitulo) {
            echo "Error al actualizar el capítulo con ID: $id_capitulo - " . mysqli_error($conn) . "<br>";
        }
    }

    // Mostrar mensaje de éxito
    $_SESSION['mensaje'] = "Capítulos actualizados correctamente";
    // Redirigir a la misma página para evitar envíos repetidos del formulario
    header("Location: series_opciones.php ");
    exit();
}
?>

<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Seleccionar Temporada y Modificar Capítulos</title>
    <link rel="stylesheet" href="css/style.css">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0/css/all.min.css" rel="stylesheet"/>
    <link rel="stylesheet" href="css/opciones.css">
</head>
<body>
    <h1>Modificar Capítulos</h1>
    

 
    
    <!-- Formulario para modificar los detalles de los capítulos -->
    <?php if ($id_temporada): ?>
        <form method="post" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>">
            <?php foreach ($capitulos as $capitulo): ?>
                <div>
                    <h2>Capítulo <?php echo $capitulo['numero_capitulo']; ?></h2>
                    <label for="titulo_<?php echo $capitulo['id_capitulo']; ?>">Título del Capítulo:</label>
                    <input type="text" id="titulo_<?php echo $capitulo['id_capitulo']; ?>" name="capitulos[<?php echo $capitulo['id_capitulo']; ?>][titulo]" value="<?php echo $capitulo['titulo']; ?>"><br><br>
                
                    <label for="descripcion_<?php echo $capitulo['id_capitulo']; ?>">Descripción del Capítulo:</label>
                    <input type="text" id="descripcion_<?php echo $capitulo['id_capitulo']; ?>" name="capitulos[<?php echo $capitulo['id_capitulo']; ?>][descripcion]" value="<?php echo $capitulo['descripcion']; ?>"><br><br>
    
                    <label for="numero_capitulo_<?php echo $capitulo['id_capitulo']; ?>">Número de Capítulo:</label>
                    <input type="text" id="numero_capitulo_<?php echo $capitulo['id_capitulo']; ?>" name="capitulos[<?php echo $capitulo['id_capitulo']; ?>][numero_capitulo]" value="<?php echo $capitulo['numero_capitulo']; ?>"><br><br>
                    
                    <!-- Botón para eliminar el capítulo -->
                    <button type="submit" name="eliminar_capitulo" value="<?php echo $capitulo['id_capitulo']; ?>">Eliminar Capítulo</button>
                    
                    <hr>
                    <br>
                </div>
            <?php endforeach; ?>
        
            <!-- Campo oculto para enviar el ID de la temporada -->
            <input type="hidden" name="id_temporada" value="<?php echo $id_temporada; ?>">
            
            <!-- Botón para guardar los cambios -->
            <input type="submit" value="Guardar Cambios">
        </form>
        <button><a href="añadir_capitulo.php?id_temporada=<?php echo $id_temporada; ?>">Agregar Nuevo Capítulo</a></button>
    <?php endif; ?>
   
    <button><a href="series_opciones.php">Volver</a></button>
</body>
</html>
