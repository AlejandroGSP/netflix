
<?php
session_start();
//include 'conexion.php';
include 'conexionDebo.php';

// Mostrar mensaje de error si está presente en la sesión
if (isset($_SESSION['error_borrar'])) {
    echo "<p>{$_SESSION['error_borrar']}</p>";
    unset($_SESSION['error_borrar']); // Limpiar el mensaje para evitar que se muestre de nuevo en futuras recargas de la página
}

// Si se ha enviado el formulario para borrar una serie
if (isset($_POST['submit_borrar']) && $_POST['submit_borrar'] == "borrar") {
    if (isset($_POST['id_serie'])) {
        $id_serie = $_POST['id_serie'];

        // Verificar si hay temporadas relacionadas
        $consulta_temporadas = "SELECT COUNT(*) AS total_temporadas FROM temporadas WHERE id_serie = '$id_serie'";
        $resultado_temporadas = mysqli_query($conn, $consulta_temporadas);
        $datos_temporadas = mysqli_fetch_assoc($resultado_temporadas);
        $total_temporadas = $datos_temporadas['total_temporadas'];

        // Verificar si hay capítulos relacionados
        $consulta_capitulos = "SELECT COUNT(*) AS total_capitulos FROM capitulos WHERE id_temporada IN (SELECT id_temporada FROM temporadas WHERE id_serie = '$id_serie')";
        $resultado_capitulos = mysqli_query($conn, $consulta_capitulos);
        $datos_capitulos = mysqli_fetch_assoc($resultado_capitulos);
        $total_capitulos = $datos_capitulos['total_capitulos'];

        if ($total_temporadas > 0 || $total_capitulos > 0) {
            // Mostrar mensaje de advertencia
            $_SESSION['mensaje'] = "Esta serie tiene temporadas o capítulos asociados. Debes eliminar primero las temporadas y los capítulos.";
            header("Location: series_opciones.php");
            exit();
        } else {
            // Eliminar la serie
            $consulta_delete = "DELETE FROM series WHERE id_serie = '$id_serie'";
            $resultado_delete = mysqli_query($conn, $consulta_delete);
            if ($resultado_delete) {
                // Redireccionar a la página index de admin
                header("Location: index_admin.php");
                exit();
            } else {
                // Si hubo un error al borrar la serie, mostrar un mensaje de error
                $_SESSION['error_borrar'] = "Error al borrar la serie.";
                header("Location: series_opciones.php");
                exit();
            }
        }
    } else {
        // Mostrar un mensaje de error
        $_SESSION['error_borrar'] = "No se ha proporcionado el ID de la serie.";
        header("Location: series_opciones.php");
        exit();
    }
}
?>

<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <title>Seleccionar Serie</title>
    <link rel="stylesheet" href="css/style.css">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0/css/all.min.css" rel="stylesheet"/>
    <link rel="stylesheet" href="css/opciones.css">

</head>
<body>
    <h1>Selecciona una Serie</h1>
    <form action="conf_serie.php" method="post">
        <?php
        // Incluir el archivo de conexión a la base de datos
        //include('conexion.php');
        include 'conexionDebo.php';

        // Consultar la base de datos para obtener todas las series
        $sql = "SELECT id_serie, titulo FROM series";
        $result = mysqli_query($conn, $sql);

        if ($result && mysqli_num_rows($result) > 0) {
            while ($row = mysqli_fetch_assoc($result)) {
                echo "<label><input type='radio' name='id_serie' value='{$row['id_serie']}'>{$row['titulo']}</label>";
            }
        } else {
            echo "<p>No hay series disponibles.</p>";
        }

        // Cerrar la conexión a la base de datos
        mysqli_close($conn);
        ?>
        <br>
        <input type="submit" value="Configurar serie">
    </form>

    <button><a href="añadir_serie.php">Añadir Serie</a></button>

    <h1>Eliminar Serie</h1>
    <form action="series_opciones.php" method="post">
        <?php
        // Incluir el archivo de conexión a la base de datos
        //include('conexion.php');
        include 'conexionDebo.php';

        $sql = "SELECT id_serie, titulo FROM series";
        $resultado = mysqli_query($conn, $sql);

        echo "<table border='1'>";
        while ($salida = mysqli_fetch_array($resultado)) {
            echo "<tr>";
            echo "<td><label><input type='radio' name='id_serie' value='{$salida['id_serie']}'>{$salida['titulo']}</label></td>"; 
            echo "<td>";
            
            // Verificar si hay temporadas o capítulos asociados
            $id_serie = $salida['id_serie'];
            $consulta_temporadas = "SELECT COUNT(*) AS total_temporadas FROM temporadas WHERE id_serie = '$id_serie'";
            $resultado_temporadas = mysqli_query($conn, $consulta_temporadas);
            $datos_temporadas = mysqli_fetch_assoc($resultado_temporadas);
            $total_temporadas = $datos_temporadas['total_temporadas'];

            $consulta_capitulos = "SELECT COUNT(*) AS total_capitulos FROM capitulos WHERE id_temporada IN (SELECT id_temporada FROM temporadas WHERE id_serie = '$id_serie')";
            $resultado_capitulos = mysqli_query($conn, $consulta_capitulos);
            $datos_capitulos = mysqli_fetch_assoc($resultado_capitulos);
            $total_capitulos = $datos_capitulos['total_capitulos'];

            if ($total_temporadas > 0 || $total_capitulos > 0) {
                echo "<p>No se puede eliminar la serie porque tiene temporadas o capítulos asociados.</p>";
            } else {
                // Mostrar el botón de eliminar si no hay temporadas ni capítulos asociados
                echo "<form method='POST' action='fxSerie.php'>";
                echo "<input type='hidden' name='id_serie' value='" . $salida['id_serie'] . "'>";
                echo "<button type='submit' name='submit_borrar' value='borrar'>Borrar</button>";
                echo "</form>";
            }
            
            echo "</td>";
            echo "</tr>";
        }
        echo "</table>";

        

        // Cerrar la conexión a la base de datos
        mysqli_close($conn);
        ?>
        
    </form>
    <button><a href="index_admin.php">Volver</a></button>

</body>
</html>
