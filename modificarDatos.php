
<?php
session_start();

//include 'conexion.php';
include 'conexionDebo.php';
// obtener el email del usuario desde el formulario
$email = $_SESSION['email'];

   //consulta como tal 
   $sql = "SELECT nombre, apellidos, email, foto FROM usuarios WHERE email='$email'";
   $resultado = mysqli_query($conn, $sql);
//    $consulta->bindParam(':email', $email);

if ($resultado && mysqli_num_rows($resultado) > 0) {
    // Obtener los datos del usuario
    $usuario = mysqli_fetch_assoc($resultado);

    // Datos del usuario
    $nombre = $usuario['nombre'];
    $apellido = $usuario['apellidos'];
    $email = $usuario['email'];
    $fotoPerfil = $usuario['foto'];

   }else {
    echo "Usuario no encontrado";
   }
    ?>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="css\modificardatos.css">
    <link rel="stylesheet" href="css\boton.css">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0/css/all.min.css" rel="stylesheet"/>
    <title>Página de Indice Admin</title>
<body>

<?php
// ACTUALIZACION DE LOS DATOS

 // Verificar si se ha enviado un formulario
if ($_SERVER["REQUEST_METHOD"] == "POST") {
    // Obtener los datos del formulario
    $nuevoNombre = $_POST['nombre']; // Obtener el nuevo nombre
    $nuevoApellido = $_POST['apellido'];
    $nuevaFoto = $_POST['fotoPerfil'];


 // Verificar si se proporciona una nueva foto
 if (!empty($nuevaFoto)) {
    // Consulta para actualizar los datos del usuario con la nueva foto
    $sql = "UPDATE usuarios SET nombre='$nuevoNombre', apellidos='$nuevoApellido', foto='img/$nuevaFoto' WHERE email='$email'";
} else {
    // Consulta para actualizar los datos del usuario sin cambiar la foto
    $sql = "UPDATE usuarios SET nombre='$nuevoNombre', apellidos='$nuevoApellido' WHERE email='$email'";
}




    // Consulta para actualizar los datos del usuario
    //$sql = "UPDATE usuarios SET nombre='$nuevoNombre', apellidos='$nuevoApellido', foto='$nuevaFoto' WHERE email='$email'";
    //$sql = "UPDATE usuarios SET nombre='$nuevoNombre', apellidos='$nuevoApellido', foto='$nuevaFoto' WHERE email='$email'";

    
    $resultado = mysqli_query($conn, $sql);

    if ($resultado) {
        echo "Datos actualizados correctamente";

    } else {
        echo "Error al actualizar los datos: " . mysqli_error($conn);
    }
}
?>

<?php
    // Mostrar mensaje de éxito si existe
    if (isset($_SESSION['mensaje'])) {
        echo "<p>{$_SESSION['mensaje']}</p>";
        unset($_SESSION['mensaje']); // Limpiar el mensaje para evitar que se muestre de nuevo en futuras recargas de la página
    }
?>




<div class="form-container">
    <h2>Modificar Datos de Usuario</h2>
    <form method="post" action="<?php echo (trim(htmlspecialchars($_SERVER["PHP_SELF"]))); ?>">

        <label for="nombre">Nombre: </label>
        <input type="text" id="nombre" name="nombre" value="<?php echo isset($nombre) ? $nombre : ''; ?>"><br><br>

        <label for="apellido">Apellido:</label>
        <input type="text" id="apellido" name="apellido"  value="<?php echo isset($apellido) ? $apellido : ''; ?>"><br><br>

        <label for="email">Correo:</label>
        <input type="email" id="email" name="email" value="<?php echo isset($email) ? $email : ''; ?>"><br><br>

        <label for="foto">Mi imagen:</label>
        <div class="image-item">
                <?php if (isset($fotoPerfil) && !empty($fotoPerfil)) { ?>
                <!--foto de perfil seleccionada previamente -->
                <input type="radio" id="opcion1" name="fotoPerfil" value="<?php echo $fotoPerfil; ?>" checked>
                <label for="opcion1"><img src="<?php echo $fotoPerfil; ?>" alt="Foto de perfil seleccionada" style="width: 100px"></label><br>
                <?php } ?>
            </div>
        <div class="image-container">
            <div class="image-item">
                <input type="radio" id="opcion1" name="fotoPerfil" value="usuarioHombre.jpg" <?php if(isset($fotoPerfil) && $fotoPerfil == 'img/usuarioHombre.jpg') echo 'checked'; ?>>
                <label for="opcion1"><img src="img/usuarioHombre.jpg" alt="Foto Hombre" style="width: 100px"></label><br>
            </div>
            <div class="image-item">
                <input type="radio" id="opcion2" name="fotoPerfil" value="usuarioMujer.jpg" <?php if(isset($fotoPerfil) && $fotoPerfil == 'img/usuarioMujer.jpg') echo 'checked'; ?>>
                <label for="opcion2"><img src="img/usuarioMujer.jpg" alt="Foto Mujer" style="width: 100px"></label><br>
            </div>
            <div class="image-item">
                <input type="radio" id="opcion3" name="fotoPerfil" value="usuarioNino.jpg" <?php if(isset($fotoPerfil) && $fotoPerfil == 'img/usuarioNino.jpg') echo 'checked'; ?>>
                <label for="opcion3"><img src="img/usuarioNino.jpg" alt="Foto Niño" style="width: 100px"></label><br>
            </div>
        </div>       

        <label for="password">Contraseña:</label>
        <input type="password" id="password" name="password" required><br><br>

        <a href="pagina_inicio.php"><input type="submit" value="Guardar Cambios"></a>

        <button><a href="javascript:history.go(-1)">Volver</a></button>
    </form>
</div>

    
</body>
</html>